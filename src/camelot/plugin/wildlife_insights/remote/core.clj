(ns camelot.plugin.wildlife-insights.remote.core
  (:require [camelot.plugin.wildlife-insights.remote.http :as http]
            [clojure.string :as cstr]))

;; test data:
;; organizations: 61, 74

;; TODO deal with unique constraint on name
;; Unique constraint on name
;;
;; {
;;   "name": "Wildlife",
;;   "streetAddress": "string",
;;   "city": "string",
;;   "state": "string",
;;   "postalCode": "string",
;;   "phone": "string",
;;   "email": "string",
;;   "countryCode": "string",
;;   "organizationUrl": "string",
;;   "remarks": "string"
;; }

(defn organizations
  ([state]
   (organizations state {}))
  ([state params]
   (http/get-data state "/api/v1/organization" params)))

(defn organization-by-id
  [state organization-id]
  (http/get-data state (str "/api/v1/organization/" organization-id)))

(defn create-organization!
  [state organization]
  (http/post state "/api/v1/organization" organization))

(defn update-organization!
  [state organization-id values]
  (http/patch state (str "/api/v1/organization/" organization-id) values))

(defn delete-organization!
  [state organization-id]
  (http/delete state (str "/api/v1/organization/" organization-id)))

;; test data
;; projects:
;; - organization 61: 133, 147
;; - organization 74: 180, 181, 182, 183

;; Project - unique constraint on name
;;
;; {:remarks nil,
;;  :slug "staging_staging_my_first_project_1562668738118",
;;  :embargo nil,
;;  :deleteDataFilesWithIdentifiedHumans nil,
;;  :methodology nil,
;;  :license nil,
;;  :name "My First Project",
;;  :startDate "2019-07-09",
;;  :acknowledgements nil,
;;  :projectStatus nil,
;;  :dataCitation nil,
;;  :organizationId 61,
;;  :shortName nil,
;;  :endDate nil,
;;  :status "CREATED",
;;  :id 133,
;;  :rightsHolder nil,
;;  :objectives nil,
;;  :projectUrl nil,
;;  :design nil,
;;  :dataUsePolicyId nil,
;;  :accessRights nil,
;;  :initiativeId nil,
;;  :trigger nil,
;;  :projectCreditLine nil,
;;  :abbreviation nil,
;;  :metadata nil}

(defn all-projects
  ([state]
   (all-projects state {}))
  ([state params]
   (http/get-data state "/api/v1/project" params)))

(defn projects
  ([state organization-id]
   (projects state organization-id {}))
  ([state organization-id params]
   (http/get-data state (str "/api/v1/organization/" organization-id
                             "/project") params)))

(defn project-by-id
  [state organization-id project-id]
  (http/get-data state (str "/api/v1/organization/" organization-id
                            "/project/" project-id)))

(defn create-project!
  [state organization-id project]
  (http/post state (str "/api/v1/organization/" organization-id
                        "/project") project))

(defn update-project!
  [state organization-id project-id values]
  (http/patch state (str "/api/v1/organization/" organization-id
                         "/project/" project-id) values))

;; Broken upstream
(defn delete-project!
  [state organization-id project-id]
  (http/delete state (str "/api/v1/organization/" organization-id
                          "/project/" project-id)))


;; deployment IDs:
;; - Project 133: 270, 271
;; - Project 147: 272

(defn all-deployments
  ([state]
   (all-deployments state {}))
  ([state params]
   (http/get-data state "/api/v1/deployment" params)))

(defn deployments
  ([state project-id]
   (deployments state project-id {}))
  ([state project-id params]
   (http/get-data state (str "/api/v1/project/" project-id
                             "/deployment") params)))

(defn deployment-by-id
  [state project-id deployment-id]
  (http/get-data state (str "/api/v1/project/" project-id
                            "/deployment/" deployment-id)))

(defn deployments-by-org-id
  [state organization-id]
  (http/get-data state (str "/api/v1/organization/" organization-id
                            "/deployment")))

(defn create-deployment!
  [state project-id deployment]
  (http/post state (str "/api/v1/project/" project-id
                        "/deployment") deployment))

(defn update-deployment!
  [state project-id deployment-id values]
  (http/patch state (str "/api/v1/project/" project-id
                         "/deployment/" deployment-id) values))

(defn delete-deployment!
  [state project-id deployment-id]
  (http/delete state (str "/api/v1/project/" project-id
                          "/deployment/" deployment-id)))


;; locations IDs:
;; - Project 133: 377
;; - Project 147: 378

;; {:habitat nil,
;;  :fieldNumber nil,
;;  :remarks nil,
;;  :secondOrderDivision nil,
;;  :plotTreatmentDescription nil,
;;  :plotTreatment nil,
;;  :placename nil,
;;  :longitude 49.9,
;;  :igbpClimateClassification nil,
;;  :id 411,
;;  :geodeticDatum "WGS84",
;;  :latitude 50,
;;  :elevationGtopo30 nil,
;;  :projectId 147,
;;  :propertyType nil,
;;  :country nil,
;;  :landcoverType nil,
;;  :firstOrderDivision nil}

(defn all-locations
  ([state]
   (all-locations state {}))
  ([state params]
   (http/get-data state "/api/v1/location" params)))

(defn locations
  ([state project-id]
   (locations state project-id {}))
  ([state project-id params]
   (http/get-data state (str "/api/v1/project/" project-id
                             "/location") params)))

(defn location-by-id
  [state project-id location-id]
  (http/get-data state (str "/api/v1/project/" project-id
                            "/location/" location-id)))

(defn locations-by-org-id
  [state organization-id]
  (http/get-data state (str "/api/v1/organization/" organization-id
                            "/location")))

(defn create-location!
  [state project-id location]
  (http/post state (str "/api/v1/project/" project-id
                        "/location") location))

(defn update-location!
  [state project-id location-id values]
  (http/patch state (str "/api/v1/project/" project-id
                         "/location/" location-id) values))

(defn delete-location!
  [state project-id location-id]
  (http/delete state (str "/api/v1/project/" project-id
                          "/location/" location-id)))

;; Data files

(defn all-data-files
  ([state]
   (all-data-files state {}))
  ([state params]
   (http/get-data state "/api/v1/data-file" params)))

(defn all-data-files-for-identification
  ([state]
   (all-data-files-for-identification state {}))
  ([state params]
   (http/get-data state "/api/v1/data-file-for-identification" params)))

(defn organization-data-files
  ([state organization-id]
   (organization-data-files state organization-id {}))
  ([state organization-id params]
   (http/get-data state (str "/api/v1/organization/" organization-id
                             "/data-file") params)))

(defn organization-data-files-for-identification
  ([state organization-id]
   (organization-data-files-for-identification state organization-id {}))
  ([state organization-id params]
   (http/get-data state (str"/api/v1/organization/" organization-id
                            "/data-file-for-identification") params)))

(defn project-data-files
  ([state project-id]
   (project-data-files state project-id {}))
  ([state project-id params]
   (http/get-data state (str "/api/v1/project/" project-id
                             "/data-file") params)))

(defn project-data-files-for-identification
  ([state project-id]
   (project-data-files-for-identification state project-id {}))
  ([state project-id params]
   (http/get-data state (str "/api/v1/project/" project-id
                             "/data-file-for-identification") params)))

(defn data-file-by-id
  [state project-id deployment-id data-file-id]
  (http/get-data state (str "/api/v1/project/" project-id
                            "/deployment/" deployment-id
                            "/data-file/" data-file-id)))

(defn data-file-download-url
  [state project-id deployment-id data-file-id]
  (http/get-data state (str "/api/v1/project/" project-id
                            "/deployment/" deployment-id
                            "/data-file/" data-file-id
                            "/download-url")))

;;{:status 201,
;; :body
;; {:canDelete false,
;;  :clientId nil,
;;  :filepath "deployment/270/72a4e179-6ddf-4f37-86f0-224d9e2ececa.file",
;;  :deploymentId 270,
;;  :thumbnailUrl
;;  "https://storage.googleapis.com/staging_staging_my_first_project_1562668738118__thumbnails/deployment/270/72a4e179-6ddf-4f37-86f0-224d9e2ececa.file",
;;  :exifDataFilePivots
;;  [{:exifTagId 1, :value 9.74, :dataFileId 10333}
;;   {:exifTagId 2, :value "JPEG", :dataFileId 10333}
;;   {:exifTagId 3, :value "image/jpeg", :dataFileId 10333}
;;   {:exifTagId 54, :value "Little-endian (Intel, II)", :dataFileId 10333}
;;   {:exifTagId 59, :value "CUDDEBACK", :dataFileId 10333}
;;   {:exifTagId 60, :value "AmbushIR", :dataFileId 10333}
;;   {:exifTagId 55, :value "1187 FW 3.1.0", :dataFileId 10333}
;;   {:exifTagId 62, :value "2014:05:29 00:03:52", :dataFileId 10333}
;;   {:exifTagId 247, :value "Copyright Cuddeback, 2014", :dataFileId 10333}
;;   {:exifTagId 61, :value "Horizontal (normal)", :dataFileId 10333}
;;   {:exifTagId 6, :value 72, :dataFileId 10333}
;;   {:exifTagId 7, :value 72, :dataFileId 10333}
;;   {:exifTagId 5, :value "inches", :dataFileId 10333}
;;   {:exifTagId 64, :value "Co-sited", :dataFileId 10333}
;;   {:exifTagId 56, :value 221, :dataFileId 10333}
;;   {:exifTagId 69, :value "2014:05:29 00:03:52", :dataFileId 10333}
;;   {:exifTagId 70, :value "2014:05:29 00:03:52", :dataFileId 10333}
;;   {:exifTagId 65, :value "1/20", :dataFileId 10333}
;;   {:exifTagId 66, :value 2.7, :dataFileId 10333}
;;   {:exifTagId 75, :value "Multi-segment", :dataFileId 10333}
;;   {:exifTagId 88, :value "Auto", :dataFileId 10333}
;;   {:exifTagId 185, :value "sRGB", :dataFileId 10333}
;;   {:exifTagId 76, :value "Fired", :dataFileId 10333}
;;   {:exifTagId 67, :value "Program AE", :dataFileId 10333}
;;   {:exifTagId 68, :value 6400, :dataFileId 10333}
;;   {:exifTagId 74, :value 0, :dataFileId 10333}
;;   {:exifTagId 57, :value 2592, :dataFileId 10333}
;;   {:exifTagId 58, :value 2000, :dataFileId 10333}
;;   {:exifTagId 78, :value "Invalid EXIF text encoding", :dataFileId 10333}
;;   {:exifTagId 180,
;;    :value
;;    "AD=12/10/2013,RD=110,LI=18,LF=10,GH=0,BT=11,BL=5638,BP=75%,CM=Still,DY=00:00:05,CF=Disabled,IR=100%,P0=0,P1=2,P2=1,P3=3,P4=3,P5=1,P6=0,P7=0",
;;    :dataFileId 10333}
;;   {:exifTagId 47, :value 2592, :dataFileId 10333}
;;   {:exifTagId 48, :value 2000, :dataFileId 10333}
;;   {:exifTagId 49, :value "Baseline DCT, Huffman coding", :dataFileId 10333}
;;   {:exifTagId 50, :value 8, :dataFileId 10333}
;;   {:exifTagId 51, :value 3, :dataFileId 10333}
;;   {:exifTagId 52, :value "YCbCr4:2:2 (2 1)", :dataFileId 10333}
;;   {:exifTagId 209, :value 2.7, :dataFileId 10333}
;;   {:exifTagId 53, :value "2592x2000", :dataFileId 10333}
;;   {:exifTagId 213, :value "1/20", :dataFileId 10333}
;;   {:exifTagId 216, :value 1.2, :dataFileId 10333}],
;;  :createdAt "2019-07-28T00:35:11.503Z",
;;  :highlighted false,
;;  :mediaType {:id 1, :mime "image/jpeg"},
;;  :filename "file",
;;  :identificationOutputs
;;  [{:confidence 0.5405333,
;;    :blankYn false,
;;    :identificationMethodId 3,
;;    :identifiedObjects
;;    [{:remarks nil,
;;      :date nil,
;;      :sex nil,
;;      :behavior nil,
;;      :relativeAge nil,
;;      :markings nil,
;;      :certainity nil,
;;      :id 8117,
;;      :identificationMetavalueId nil,
;;      :commonName nil,
;;      :identificationId 11354,
;;      :taxonomyId 5225,
;;      :individualIdentified false}],
;;    :dataFileId 10333,
;;    :participantId nil,
;;    :id 11354,
;;    :timestamp "2019-07-28T00:35:11.503Z"}],
;;  :status "CV",
;;  :mediaTypeId 1,
;;  :id 10333,
;;  :filesize 1092108,
;;  :timestamp "2019-07-19T00:00:00.000Z",
;;  :participantId 40}}
;;
;;{:canDelete true,
;; :clientId nil,
;; :filepath "deployment/270/1c2ebdaf-0785-42b4-877b-65735f4a085d.jpg",
;; :deploymentId 270,
;; :thumbnailUrl
;; "https://storage.googleapis.com/staging_staging_my_first_project_1562668738118__thumbnails/deployment/270/1c2ebdaf-0785-42b4-877b-65735f4a085d.jpg",
;; :createdAt "2019-07-28T01:00:52.780Z",
;; :highlighted false,
;; :filename "myfile.jpg",
;; :status "VERIFIED",
;; :mediaTypeId 1,
;; :id 10344,
;; :filesize "1092108",
;; :timestamp "2019-07-20T00:00:00.000Z",
;; :participantId 40}

(defn create-data-file!
  [state project-id deployment-id {:keys [filename timestamp mime-type]}]
  (http/post-multipart
   state (str "/api/v1/project/" project-id
              "/deployment/" deployment-id
              "/data-file")
   {:multipart
    [{:part-name "file"
      :name (.getName (clojure.java.io/file filename))
      :content (clojure.java.io/file filename)
      :mime-type mime-type}
     {:name "timestamp" :content timestamp}]}))

(defn update-data-file!
  [state project-id deployment-id data-file-id data-file]
  (http/patch state (str "/api/v1/project/" project-id
                         "/deployment/" deployment-id
                         "/data-file/" data-file-id)
              data-file))

(defn delete-data-file!
  [state project-id deployment-id data-file-id]
  (http/delete state (str "/api/v1/project/" project-id
                         "/deployment/" deployment-id
                         "/data-file/" data-file-id)))

;; Identification outputs

(defn identification-outputs
  ([state project-id deployment-id data-file-id]
   (identification-outputs state project-id deployment-id data-file-id {}))
  ([state project-id deployment-id data-file-id params]
   (http/get-data
    state (str "/api/v1/project/" project-id
               "/deployment/" deployment-id
               "/data-file/" data-file-id
               "/identification-output")
    params)))

(defn identification-output
  ([state project-id deployment-id data-file-id identification-output-id]
   (identification-output state project-id deployment-id data-file-id
                          identification-output-id {}))
  ([state project-id deployment-id data-file-id identification-output-id params]
   (http/get-data
    state (str "/api/v1/project/" project-id
               "/deployment/" deployment-id
               "/data-file/" data-file-id
               "/identification-output/" identification-output-id))))

(defn create-identification-output!
  [state project-id deployment-id data-file-id identification-output]
  (http/post
   state (str "/api/v1/project/" project-id
              "/deployment/" deployment-id
              "/data-file/" data-file-id
              "/identification-output")
   identification-output))

(defn update-identification-output!
  [state project-id deployment-id data-file-id
   identification-output-id identification-output]
  (http/patch state (str "/api/v1/project/" project-id
                         "/deployment/" deployment-id
                         "/data-file/" data-file-id
                         "/identification-output/" identification-output-id)
              identification-output))

(defn delete-identification-output!
  [state project-id deployment-id data-file-id identification-output-id]
  (http/delete state (str "/api/v1/project/" project-id
                         "/deployment/" deployment-id
                         "/data-file/" data-file-id
                         "/identification-output/" identification-output-id)))

;; identification objects

(defn identified-objects
  ([state project-id deployment-id data-file-id identification-output-id]
   (identified-objects state project-id deployment-id data-file-id
                           identification-output-id {}))
  ([state project-id deployment-id data-file-id identification-output-id
    params]
   (http/get-data
    state (str "/api/v1/project/" project-id
               "/deployment/" deployment-id
               "/data-file/" data-file-id
               "/identification-output/" identification-output-id
               "/identified-object")
    params)))

(defn identified-object
  ([state project-id deployment-id data-file-id
    identification-output-id identified-object-id]
   (identified-object state project-id deployment-id data-file-id
                          identification-output-id
                          identified-object-id {}))
  ([state project-id deployment-id data-file-id
    identification-output-id identified-object-id params]
   (http/get-data
    state (str "/api/v1/project/" project-id
               "/deployment/" deployment-id
               "/data-file/" data-file-id
               "/identification-output/" identification-output-id
               "/identified-object/" identified-object-id))))

(defn create-identified-object!
  [state project-id deployment-id data-file-id identification-output-id
   identified-object]
  (http/post
   state (str "/api/v1/project/" project-id
              "/deployment/" deployment-id
              "/data-file/" data-file-id
              "/identification-output/" identification-output-id
              "/identified-object")
   identified-object))

(defn update-identified-object!
  [state project-id deployment-id data-file-id
   identification-output-id identified-object-id
   identified-object]
  (http/patch state (str "/api/v1/project/" project-id
                         "/deployment/" deployment-id
                         "/data-file/" data-file-id
                         "/identification-output/" identification-output-id
                         "/identified-object/" identified-object-id)
              identified-object))

(defn delete-identified-object!
  [state project-id deployment-id data-file-id
   identification-output-id identified-object-id]
  (http/delete state (str "/api/v1/project/" project-id
                         "/deployment/" deployment-id
                         "/data-file/" data-file-id
                         "/identification-output/" identification-output-id
               "/identified-object/" identified-object-id)))


;; taxonomy

(defn taxonomy
  [state genus species]
  (->>  {:search (str genus " " species)}
        (http/get-data state (str "/api/v1/taxonomy"))
        :body
        :data
        (filter #(and (= (cstr/lower-case (:genus %)) (cstr/lower-case genus))
                      (= (cstr/lower-case (:species %)) (cstr/lower-case species))
                      (= (:taxonLevel "species"))))
        first))

(defn taxonomy-by-id
  [state taxonomy-id]
  (http/get-data state (str "/api/v1/taxonomy/" taxonomy-id)))

;; participant type

;; broken
(defn participant-types
  ([state]
   (participant-types state {}))
  ([state params]
   (http/get-data state "/api/v1/participant-type" params)))

;; data-use-policy

(defn data-use-policies
  ([state]
   (data-use-policies state {}))
  ([state params]
   (http/get-data state "/api/v1/data-use-policy" params)))

(defn create-data-use-policy!
  ([state params]
   (http/post state "/api/v1/data-use-policy" params)))

(defn update-data-use-policy!
  ([state policy-id params]
   (http/patch state (str "/api/v1/data-use-policy/" policy-id) params)))

(ns camelot.plugin.wildlife-insights.remote.http
  (:require [clj-http.client :as http]
            [clj-http.conn-mgr :as conn-mgr]
            [cheshire.core :as cheshire]
            [clojure.string :as cstr]
            [clojure.tools.logging :as log]
            [diehard.core :as dh])
  (:import [net.jodah.failsafe CircuitBreakerOpenException]))

(defonce connection-manager (conn-mgr/make-reusable-conn-manager {}))
(defonce client (atom nil))
(defonce tokens (atom {}))

(defonce get-config
  {:content-type :json
   :connection-manager connection-manager
   :cache true
   :socket-timeout 2000
   :connection-timeout 2000})

(dh/defretrypolicy retry-policy
  {:max-retries 5
   :backoff-ms [1000 10000 2.0]
   :jitter-factor 0.1})

(dh/defcircuitbreaker http
  {:failure-threshold-ratio [40 50]
   :success-threshold-ratio [5 8]
   :delay-ms 60000})

(declare auth)

(defn- error-from-response
  [r]
  (try
    (-> r
        :body
        (cheshire/parse-string true)
        :errors
        first)
    (catch Exception e
      {:message (:body r)})))

(defn handle-exception
  [state e]
  (let [status (:status (ex-data e))]
    (condp = status
      400 {:status status
           :body (error-from-response (ex-data e))}
      401 (do (auth state)
              (throw e))
      403 {:status status}
      404 {:status status}
      (throw e))))

(defmacro handle-request [state body]
  `(try
     (dh/with-retry (:retry-strategy ~state)
       (try
         (dh/with-circuit-breaker http
           (let [response# ~body]
             {:status (:status response#)
              :body (cheshire/parse-string (:body response#) true)}))
         (catch Exception e#
               (handle-exception ~state e#))
         (catch CircuitBreakerOpenException _#
           {:state :circuit-breaker-open})))
     (catch Exception e#
       {:status (:status (ex-data e#))
        :body (error-from-response (ex-data e#))})))

(defn post
  [{:keys [hostname email password retry-strategy] :as state} url body]
  (handle-request state
   (http/post (str "https://" hostname url "?token=" (get @tokens email))
              {:content-type :json
               :body (cheshire/generate-string body)
               :socket-timeout 60000
               :connection-timeout 60000
               :accept :json})))

(defn auth
  [{:keys [email password] :as state}]
  (let [response (post state "/v1/auth/sign-in"
                       {:email email
                        :password password})]
    (if-let [token (get-in response [:body :token])]
      (swap! tokens assoc email token)
      (throw (ex-info "Token not found in response" {:response response})))))

(defn post-multipart
  [{:keys [hostname email password retry-strategy] :as state} url body]
  (handle-request state
   (http/post (str "https://" hostname url "?token=" (get @tokens email))
              (merge {:socket-timeout 60000
                      :connection-timeout 60000}
                     body))))

(defn patch
  [{:keys [hostname email password retry-strategy] :as state} url body]
  (handle-request state
   (http/patch (str "https://" hostname url "?token=" (get @tokens email))
               {:content-type :json
                :body (cheshire/generate-string body)
                :socket-timeout 2000
                :connection-timeout 2000
                :accept :json})))

(defn delete
  [{:keys [hostname email password retry-strategy] :as state} url]
  (handle-request state
                  (http/delete (str "https://" hostname url "?token=" (get @tokens email))
                               {:socket-timeout 2000
                                :connection-timeout 2000})))

(defn- to-query-params
  [params]
  (let [ps (cstr/join "&"
                      (reduce-kv (fn [acc p v] (conj acc (str (name p) "=" v)))
                                 []
                                 params))]
    (if (empty? ps)
      ps
      (str "&" ps))))

(defn get-data
  ([state url]
   (get-data state url {}))
  ([{:keys [hostname email password retry-strategy] :as state} url params]
   (handle-request state
                   (http/get (str "https://" hostname url "?token=" (get @tokens email)
                                  (to-query-params params))
                             (if-let [cl @client]
                               (assoc get-config :http-client cl)
                               get-config)))))

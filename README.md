# Camelot Plugin: Wildlife Insights

This is a plugin for Camelot to integrate with with https://wildlifeinsights.org.

This has two key benefits:

- Allows conservation decisions to be made globally with the very latest data
  on-hand.
- Leverage Machine Learning to get classification suggestions for your data.

## Usage

This plugin is in development. It is not yet ready for use.

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
